# The installation steps

1.  `pipenv install`
2.  `pipenv shell`
3.  `python manage.py migrate`
4. `coverage run manage.py test`
5. `coverage report`

## How do app handler ?

1. `Locating the home page`
2. `Locating the importing page, here You can import an Excel file is in a repository named prueba.xlsx`
3. `Locating the list page, It shows register from database`
4. `In the URL /list sale/ You can see how I applicate Django Rest Framework with HTTP Verbs (GET, POST, PUT DELETE)`
5. `Locating In the Produced Total page, It can calculate the produced to convert a foreign currency(USD, EUR, MXN pesos) through scraping.`

# My workshop of Python Architecture is in the Master branch.
