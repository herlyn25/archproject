import requests
from bs4 import BeautifulSoup
from dateutil.parser import parse
from inventory.forms import SaleForm
from inventory.models import Sale


class Functions:

    def save_product_bd(self, item):
        sale = Sale()
        sale.region = item['Region']
        sale.country = item['Country']
        sale.item_type = item['Item Type']
        sale.order_date = parse(str(item['Order Date']))
        sale.order_id = item['Order ID']
        sale.units_sold = int(item['Units Sold'])
        sale.unit_price = float(item['Unit Price'])
        sale.unit_cost = float(item['Unit Cost'])

        data = {
            'region': sale.region,
            'country': sale.country,
            'item_type': sale.item_type,
            'order_date': sale.order_date.date(),
            'order_id': sale.order_id,
            'units_sold': sale.units_sold,
            'unit_price': sale.unit_price,
            'unit_cost': sale.unit_cost
        }

        form = SaleForm(data)

        if form.is_valid():
            form.cleaned_data
            form.save()
            return True

