from django.urls import path
from inventory.api import SaleApiView, detail_sale_apiview
from inventory.views import importer_sales, list_sales, total_produced, home

urlpatterns = [
    path('', home, name='home'),
    path('import/', importer_sales, name='import'),
    path('listsales/', list_sales, name='listsales'),
    path('listsale/', SaleApiView.as_view(), name='listsale'),
    path('listsale/<int:pk>/', detail_sale_apiview, name='detailsale'),
    path('produced/', total_produced, name='produced'),
]
