from django.db import models


# Create your models here.
class Sale(models.Model):
    region = models.CharField(max_length=50, null=False, blank=False, default=None)
    country = models.CharField(max_length=50, null=False, blank=False, default=None)
    item_type = models.CharField(max_length=50, null=False, blank=False, default=None)
    order_date = models.DateField()
    order_id = models.IntegerField(unique=True)
    units_sold = models.IntegerField()
    unit_price = models.FloatField()


class Document(models.Model):
    name = models.CharField(max_length=255)
    document = models.FileField(upload_to='file/')
    creation_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.document
