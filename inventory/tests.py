from unittest import TestCase
from django.urls import include, path, reverse
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase

from helpers.functions import Functions
from inventory.forms import SaleForm
from inventory.models import Sale
from django.db.utils import IntegrityError
from django.test import TransactionTestCase, Client


class TestSale(TransactionTestCase):
    def test_unique_order_id(self):
        count_sale = Sale(region="Asia", country="China", item_type="Fruits", order_id=4657,
                          order_date='2022-04-12', units_sold=657, unit_price=67839)
        count_sale.save()
        count_sale2 = Sale(region="Asia", country="Japon", item_type="Bread", order_id=4657,
                           order_date='2022-04-11', units_sold=147, unit_price=839)
        with self.assertRaises(IntegrityError, msg='UNIQUE constraint failed: inventory_sale.order_id'):
            count_sale2.save()

    def test_order_date_required(self):
        count_sale = Sale(region="Asia", country="China", item_type="Fruits", order_id=4657,
                          units_sold=657, unit_price=67839)
        with self.assertRaises(IntegrityError, msg='NOT NULL constraint failed: inventory_sale.order_date'):
            count_sale.save()

    def test_region_required(self):
        count_sale = Sale(country="China", item_type="Fruits", order_id=4657,
                          order_date='2022-04-11', units_sold=657, unit_price=67839)
        with self.assertRaises(IntegrityError, msg='NOT NULL constraint failed: inventory_sale.region'):
            count_sale.save()

    def test_country_required(self):
        count_sale = Sale(region="Asia", item_type="Fruits", order_id=4657,
                          order_date='2022-04-11', units_sold=657, unit_price=67839)
        with self.assertRaises(IntegrityError, msg='NOT NULL constraint failed: inventory_sale.country'):
            count_sale.save()

    def test_item_type_required(self):
        count_sale = Sale(region="Asia", country="Israel", order_id=4657,
                          order_date='2022-04-11', units_sold=657, unit_price=67839)
        with self.assertRaises(IntegrityError, msg='NOT NULL constraint failed: inventory_sale.country'):
            count_sale.save()


class TestSaleApiView(APITestCase, URLPatternsTestCase):
    urlpatterns = [
        path('', include('inventory.urls'))
    ]

    def test_create_sale(self):
        """
        You can make sure that You create a new sale
        """
        url = reverse('listsale')
        data = {'region': "Asia", 'country': "China", 'item_type': "Fruits", 'order_id': 4657,
                'order_date': '2022-04-12', 'units_sold': 657, 'unit_price': 67839}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_list_sale(self):
        """
        You can make sure list all sales
        """
        url = reverse('listsale')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_sale(self):
        """
        You can make sure update a sale
        """
        sale = Sale.objects.create(region="Asia", country="China", item_type="Fruits", order_id=4657,
                                   order_date='2022-04-12', units_sold=657, unit_price=67839)

        sale_update = {'region': 'Asia', 'country': 'China', 'item_type': 'Cereal', 'order_id': 4657,
                       'order_date': '2022-04-12', 'units_sold': 657, 'unit_price': 67839}

        response = self.client.put(f'/listsale/{sale.pk}/', sale_update, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_sale(self):
        """
        You can make sure delete a sale
        """
        sale = Sale.objects.create(region="Asia", country="China", item_type="Fruits", order_id=4657,
                                   order_date='2022-04-12', units_sold=657, unit_price=67839)

        response = self.client.delete(f'/listsale/{sale.pk}/', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)





class TestViews(TestCase):

    def test_home(self):
        client = Client()
        response = client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_list_sale(self):
        client = Client()
        response = client.get('/listsales/')
        self.assertEqual(response.status_code, 200)

    def test_importer_get(self):
        client = Client()
        response = client.get('/import/')
        self.assertEqual(response.status_code, 200)

    def test_total_produced(self):
        client = Client()
        response = client.get('/produced/')
        self.assertEqual(response.status_code, 200)


class TestFunctions(TestCase):

    def test_save_product(self):
        function = Functions()
        keys = ['Region', 'Country', 'Item Type', 'Order Date', 'Order ID', 'Units Sold', 'Unit Price', 'Unit Cost']

        sale = {'Region': 'asia',
                'Country': 'Japon',
                'Item Type': "Frutas",
                'Order Date': "2022-05-05",
                'Order ID': 1,
                'Units Sold': 10,
                'Unit Price': 200,
                'Unit Cost': 150
                }
        keys_sale = [x for x in sale.keys()]

        self.assertEqual(keys, keys_sale)
        self.assertEqual(dict, type(sale))
        self.assertTrue(function.save_product_bd(sale))


class TestSaleForm(TestCase):
    def test_form_sale(self):
        data = {'region': "Asia", 'country': "China", 'item_type': "Fruits", 'order_id': 4657,
                'order_date': '2022-04-12', 'units_sold': 657, 'unit_price': 67839}
        form = SaleForm(data)
        self.assertTrue(form.is_valid())
