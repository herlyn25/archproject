#import pathlib
from django.db.models import Sum
from django.shortcuts import render, redirect
from django.core.paginator import Paginator

from archproject import settings
#from helpers.functions import Functions
from inventory.forms import DocumentForm
from inventory.models import Sale, Document
import requests
from bs4 import BeautifulSoup

# Create your views here.
from inventory.tasks import task_save_data


def home(request):
    return render(request, 'home.html')


def importer_sales(request):
    form = DocumentForm()
    context={}
    if request.method == 'POST':
        #file = request.FILES['document']
        form = DocumentForm(request.POST, request.FILES)
        #ext = pathlib.Path(file.name).suffix
        form.save()
        doc = Document.objects.all().reverse().first()
        task_save_data.delay(f'{settings.MEDIA_ROOT}/{doc.document}')
        return redirect('/')
    context = {'form': form}

    return render(request, 'export_file.html', context)


def list_sales(request):
    entity = Sale.objects.all().order_by('order_id')
    page = request.GET.get('page', 1)
    paginator = None
    try:
        paginator = Paginator(entity, 25)
        entity = paginator.page(page)
    except TypeError:
        print("Don't found paginator")
    context = {'entity': entity, 'paginator': paginator}
    return render(request, 'list_sales.html', context)


def total_produced(request):
    phrase_conversion = ''
    if request.method == 'POST':
        currency = request.POST['currency']
        print(type(currency))
        sum = Sale.objects.all().aggregate(Sum('unit_price'), Sum('units_sold'))
        url = 'https://www.google.com/search'
        params = {'q': f'1 {currency} to cop'}
        response = requests.get(url, params=params)
        soup = BeautifulSoup(response.text, 'html.parser')
        element = soup.select_one('.BNeawe.iBp4i.AP7Wnd')
        str_value, *_ = element.text.split(' ')
        value = float(str_value.replace(',', ''))
        total_local_currency = sum['unit_price__sum'] * sum['units_sold__sum']
        total_foreign_currency = value * total_local_currency
        phrase_conversion = f'{total_local_currency} COP Produced Total equals {total_foreign_currency} {currency.upper()}'

    context = {'phrase': phrase_conversion}
    return render(request, 'convert_total.html', context)
