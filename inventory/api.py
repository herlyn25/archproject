from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from inventory.models import Sale
from inventory.serializers import SaleSerializer


class SaleApiView(APIView):
    """
    This class create and list serialized objects with HTTP methods get and post.
    """

    def get(self, request):
        file_excel = Sale.objects.all()
        file_excel_serializer = SaleSerializer(file_excel, many=True)
        return Response(file_excel_serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        file_excel_serializer = SaleSerializer(data=request.data)
        if file_excel_serializer.is_valid():
            file_excel_serializer.save()
            return Response('Sale was created success', status=status.HTTP_201_CREATED)
        return Response(file_excel_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def detail_sale_apiview(request, pk):
    """
    :param request: It handles the HTTP methods and requests.
    :param pk: Primary key from table sale in Database.
    :return: HTTP Response to render a JSON file.
    """
    sale = Sale.objects.filter(id=pk).first()
    if sale:
        if request.method == "GET":
            sale_serializer = SaleSerializer(sale)
            return Response(sale_serializer.data, status=status.HTTP_200_OK)
        elif request.method == "PUT":
            sale_serializer = SaleSerializer(sale, data=request.data)
            if sale_serializer.is_valid():
                sale_serializer.save()
                return Response(sale_serializer.data, status=status.HTTP_200_OK)
            return Response(sale_serializer.errors)
        elif request.method == "DELETE":
            sale.delete()
            return Response(f'Sale {pk} was deleted', status=status.HTTP_200_OK)
    else:
        return Response({'message': f"Don't was found the sale with id {pk}"}, status=status.HTTP_400_BAD_REQUEST)
