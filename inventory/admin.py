from django.contrib import admin

# Register your models here.
from inventory.models import Sale, Document


@admin.register(Sale)
class FileExcelAdmin(admin.ModelAdmin):
    list_display = ['order_id', 'region', 'country', 'item_type', 'order_date',
                    'units_sold', 'unit_price']


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ['id', 'document', 'creation_date']
