from datacleaner import autoclean

import pandas as pd

from archproject.celery import app
from helpers.functions import Functions

functions = Functions()


@app.task
def task_save_data(path):
    df_list = []
    df = pd.read_excel(path)
    df.fillna("", inplace=True)
    columns_dict = [x for x in df.columns]
    for element in df.index.values:
        df_line = df.loc[element, columns_dict].to_dict()
        df_list.append(df_line)
    print(df_list)
    for item in df_list:
        functions.save_product_bd(item)
