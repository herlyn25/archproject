from django import forms
from inventory.models import Sale, Document


class SaleForm(forms.ModelForm):
    region = forms.CharField(initial='class')
    country = forms.CharField(initial='class')
    item_type = forms.CharField(initial='class')
    order_date = forms.DateField()
    order_id = forms.IntegerField()
    units_sold = forms.IntegerField()
    unit_price = forms.FloatField()

    class Meta:
        model = Sale
        exclude = ['id']

class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ['name','document']


class CurrencyForm(forms.ModelForm):
    CURRENCY_CHOICES = (('usd', 'Dolar'), ('eur', 'Euro'), ('mxn', 'Mexican Pesos'))
    currency = forms.ChoiceField(choices=CURRENCY_CHOICES)
